extends Area2D

@export var speed = 500;


func _ready():
	var initial_scale = scale
	var tween = create_tween()
	tween.tween_property(self, "scale", initial_scale, 0.1).from(scale * 0)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	position.y -= speed * delta
