extends Control

var level_scene: PackedScene = load("res://scenes/level.tscn")
var can_restart := false


func _ready():
	$StartAgainMessage.hide()
	$VBoxContainer/Score/MarginContainer/HBoxContainer/ScoreLabel.text = str(Global.score)


func _input(event):
	if can_restart and event.is_action_pressed("shoot"):
		get_tree().change_scene_to_packed(level_scene)


func _on_start_again_timer_timeout():
	$StartAgainMessage.show()
	can_restart = true
