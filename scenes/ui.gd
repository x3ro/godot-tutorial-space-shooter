extends CanvasLayer

var health_texture: Resource
var health_icon: TextureRect
var score := 0  # TODO: Score should be held by the level/game. I left it here because the tutorial might use it to show some concept later on.


func _ready():
	health_icon = $Stats/HealthBar.get_child(0)
	$Stats/HealthBar.remove_child(health_icon)
	Global.score = 0  # TODO: I think handling the score should be done in the lavel/game logic, not in the UI. I'll leave it here because the tutorial might use it to show some concept later on.


func set_health(amount: int):
	for child in $Stats/HealthBar.get_children():
		child.queue_free()

	for i in amount:
		$Stats/HealthBar.add_child(health_icon.duplicate())


func _on_score_timer_timeout():
	increase_score()


func increase_score():
	Global.score += 1
	$Score/Label.text = str(Global.score)
