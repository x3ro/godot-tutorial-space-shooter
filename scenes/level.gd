extends Node2D

@export var MIN_STARS_PER_TICK := 1
@export var MAX_STARS_PER_TICK := 5
@export var STAR_SPEED := 50.0

var meteor_scene: PackedScene = load("res://scenes/meteor.tscn")
var laser_scene: PackedScene = load("res://scenes/laser.tscn")
var star_1_scene: PackedScene = load("res://scenes/star_1.tscn")
var player_health := 5

@onready var rng = RandomNumberGenerator.new()


func _ready():
	simulate_star_creation(100)
	update_ui_player_health()


func simulate_star_creation(steps: int):
	for i in steps:
		$StarSpawner._on_spawn_timer_timeout()
		for star in $Stars.get_children():
			star._process($StarSpawner/SpawnTimer.wait_time)


func update_ui_player_health():
	get_tree().call_group("ui", "set_health", player_health)


func _on_meteor_spawner_new_instance(meteor):
	meteor.connect("collision", on_meteor_collision)
	meteor.connect("left_window", on_meteor_left_window)


func on_meteor_collision():
	if $Player.is_invincible:
		return

	if not $Player.god_mode:
		player_health -= 1
	update_ui_player_health()
	$Player.set_invincible(1.5)
	$Player.play_damage_sound()
	if player_health <= 0:
		get_tree().call_deferred("change_scene_to_file", "res://scenes/game_over_screen.tscn")


func on_meteor_left_window(meteor):
	$Meteors.remove_child(meteor)


func _on_player_laser(pos: Vector2):
	var laser: Node2D = laser_scene.instantiate()
	$Lasers.add_child(laser)
	laser.position = pos


func _on_star_spawner_new_instance(star):
	star.speed = STAR_SPEED
	star.randomize_distance()
