extends Node2D

signal left_window(node: Node2D)

@export var speed := 50.0
@export var twinkle_duration := 1.0
## Distance for parralax: 0 = nearest, 1 = farthest
@export_range(0.0, 1.0) var distance := 0.0:
	get:
		return __distance
	set(value):
		__distance = value
		__speed_factor = pow(1 - value, 2)
var __distance := 1.0
var __speed_factor := 1.0

var rng: RandomNumberGenerator
var twinkle_scale: Vector2
var normal_scale: Vector2
var can_twinkle := true


func _init():
	rng = RandomNumberGenerator.new()


func _process(delta):
	twinkle()

	position.y += speed * __speed_factor * delta


func twinkle():
	if not can_twinkle or twinkle_duration <= 0.0:
		return

	can_twinkle = false

	var sprite_scale = $StarImage.scale
	var tween = create_tween()
	tween.tween_property($StarImage, "scale", sprite_scale * twinkle_scale, twinkle_duration / 2)
	await tween.tween_property($StarImage, "scale", sprite_scale, twinkle_duration / 2).finished

	can_twinkle = true


func randomize_distance():
	twinkle_duration = rng.randf_range(0.4, 1.6)
	twinkle_scale = Vector2(1.0, 1.0) * rng.randf_range(0.5, 0.9)

	distance = rng.randf_range(0.0, 0.5)
	scale = Vector2(1.0, 1.0) * (1 - distance)


func _on_visible_on_screen_notifier_2d_screen_exited():
	queue_free()
