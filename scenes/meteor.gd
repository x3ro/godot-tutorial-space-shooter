extends Area2D

signal left_window(node: Node2D)
signal collision

var angular_momentum: float
var speed: int
var direction: Vector2
var can_collide := true


func _ready():
	var rng := RandomNumberGenerator.new()

	var random_rotation = randf_range(0, 2 * PI)
	rotate(random_rotation)

	angular_momentum = rng.randf_range(-0.2, 0.2)
	speed = rng.randi_range(200, 500)

	var direction_x = rng.randf_range(-0.5, 0.5)
	direction = Vector2(direction_x, 1).normalized()


func _process(delta):
	position += direction * speed * delta
	rotate(angular_momentum * 2 * PI * delta)

	if position.y > get_viewport_rect().size.y + 100:
		left_window.emit(self)


func _on_body_entered(_body):
	if not can_collide:
		return
	collision.emit()


func _on_area_entered(area):
	# Prevent other lasers from hitting
	if not can_collide:
		return
	can_collide = false

	area.queue_free()
	$MeteorImage.hide()

	$DestructionSound.play()
	# HACK: The audio file is longer than 0.5 s. The timer below is actuallu just a workaround so
	#       that the audio has a chance to play before the scene is destroyed but it has the side
	#       effect, that the reverb in the audio file is cut off. When this is changed to only
	#       destroy the scene once the audio has finished playing, the audiofile will have to be
	#       trimmed to remove the reverb.
	await get_tree().create_timer(0.5).timeout
	queue_free()
