extends ReferenceRect

signal new_instance(instance: Node2D)

@export var ACTIVE := true:
	set(value):
		$SpawnTimer.paused = !value
	get:
		return not $SpawnTimer.paused

@export var SPAWN_TIMEOUT_MIN := 0.0
@export var SPAWN_TIMEOUT_MAX := 1.0
@export var SPAWN_ENTITIES_MIN := 1
@export var SPAWN_ENTITIES_MAX := 2

@export var SPAWN_NODE: Node2D

@onready var rng := RandomNumberGenerator.new()

var spawnable_instances: Array[Node2D]


func _ready():
	randomize_timeout()

	# Storea all children that are Node2D
	for child in get_children():
		if not child is Node2D:
			continue
		spawnable_instances.append(child)
		remove_child(child)


func _on_spawn_timer_timeout():
	randomize_timeout()

	var num_new_instances := rng.randi_range(SPAWN_ENTITIES_MIN, SPAWN_ENTITIES_MAX)
	for i in num_new_instances:
		spawn_instance()


func spawn_instance():
	var instance = create_random_instance()
	instance.position = global_position + Vector2(size.x * rng.randf(), size.y * rng.randf())
	SPAWN_NODE.add_child(instance)
	new_instance.emit(instance)


func create_random_instance():
	var idx := rng.randi_range(0, len(spawnable_instances) - 1)
	return spawnable_instances[idx].duplicate()


func randomize_timeout():
	var rnd = rng.randf_range(SPAWN_TIMEOUT_MIN, SPAWN_TIMEOUT_MAX)
	$SpawnTimer.start(rnd)
