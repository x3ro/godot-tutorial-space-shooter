extends CharacterBody2D

@export var god_mode := false
@export var movement_speed := 200

signal laser(pos: Vector2)

var can_shoot := true
var invincibility_tween: Tween
var is_invincible: bool:
	get:
		return $InvincibleTimer.time_left > 0


func _ready():
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	process_movement()
	process_shooting()


func process_movement():
	var direction = Input.get_vector("left", "right", "up", "down")
	if Input.is_action_pressed("boost"):
		direction *= 2
	velocity = direction * movement_speed  # No need to multiply `delta`, Godot does it for us
	move_and_slide()  # Apply velocity to get movement


func process_shooting():
	if not can_shoot:
		return

	if Input.is_action_just_pressed("shoot"):
		can_shoot = false
		$LaserTimer.start()
		laser.emit($LaserStartPos.global_position)
		$LaserSound.play()


func _on_laser_timer_timeout():
	can_shoot = true


func play_damage_sound():
	$DamageSound.play()


func set_invincible(time_sec: float):
	if not is_invincible:
		invincibility_tween = create_tween()
		invincibility_tween.set_loops()  # Run infinitely
		invincibility_tween.tween_property($PlayerImage, "modulate:a", 0.25, 0.125)
		invincibility_tween.tween_property($PlayerImage, "modulate:a", 1.0, 0.125)
	$InvincibleTimer.start(time_sec)


func _on_invincible_timer_timeout():
	if invincibility_tween:
		invincibility_tween.pause()
		$PlayerImage.modulate.a = 1.0
